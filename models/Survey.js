const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const recipientSchema = require("./Recipient");

const surveySchema = new Schema({
  title: String,
  body: String,
  subject: String,
  // recipient 'sub document' collection - recipients only useful underneath survey
  recipients: [recipientSchema],
  yes: { type: Number, default: 0 },
  no: { type: Number, default: 0 },
  // underscore denotes reference or relationship field
  _user: { type: Schema.Types.ObjectId, ref: 'User' },
  dateSent: Date,
  lastResponded: Date
});

mongoose.model("surveys", surveySchema);
