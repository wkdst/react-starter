import { combineReducers } from "redux";
import authReducer from "./authReducer";

// This exports the combined reducers as 'reducers'
export default combineReducers({
  auth: authReducer,
  
});
